#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <thread>
#include <mutex>
#include <PlayerInfo.h>

std::mutex mtx;


#define MAX 100
#define SIZE_TABLERO 64
#define SIZE_FILA_TABLERO 8
#define LADO_CASILLA 64
#define RADIO_AVATAR 25.f
#define OFFSET_AVATAR 5

#define SIZE_TABLERO 64
#define LADO_CASILLA 64
#define RADIO_AVATAR 25.f
#define OFFSET_AVATAR 5



sf::Vector2f BoardToWindows(sf::Vector2f _position)
{
	return sf::Vector2f(_position.x*LADO_CASILLA + OFFSET_AVATAR, _position.y*LADO_CASILLA + OFFSET_AVATAR);
}

void DibujaSFML()
{
	sf::Vector2f casillaOrigen, casillaDestino;
	bool casillaMarcada = false;

	sf::RenderWindow window(sf::VideoMode(512, 512), "Ejemplo tablero");
	while (window.isOpen())
	{
		sf::Event event;

		//Este primer WHILE es para controlar los eventos del mouse
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			default:
				break;

			}
		}

		window.clear();

		//A partir de aquÃ­ es para pintar por pantalla
		//Este FOR es para el tablero
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				sf::RectangleShape rectBlanco(sf::Vector2f(LADO_CASILLA, LADO_CASILLA));
				rectBlanco.setFillColor(sf::Color::White);
				if (i % 2 == 0)
				{
					//Empieza por el blanco
					if (j % 2 == 0)
					{
						rectBlanco.setPosition(sf::Vector2f(i*LADO_CASILLA, j*LADO_CASILLA));
						window.draw(rectBlanco);
					}
				}
				else
				{
					//Empieza por el negro
					if (j % 2 == 1)
					{
						rectBlanco.setPosition(sf::Vector2f(i*LADO_CASILLA, j*LADO_CASILLA));
						window.draw(rectBlanco);
					}
				}
			}
		}

		//Para pintar el un circulito
		sf::CircleShape shape(RADIO_AVATAR);
		shape.setFillColor(sf::Color::Blue);
		sf::Vector2f posicion_bolita(4.f, 7.f);
		posicion_bolita = BoardToWindows(posicion_bolita);
		shape.setPosition(posicion_bolita);
		window.draw(shape);
		window.display();
	}

}


void threadFunction(sf::TcpSocket* sock/*, std::vector<std::string> *v*/)
{
	while (true)
	{
		sf::Packet pcket;
		sock->receive(pcket);
		sf::String str;
		pcket >> str;
		mtx.lock();
		std::string a = str;
		std::cout << a << std::endl;
		//v->push_back(str);
		mtx.unlock();
	}
}

int main()
{
	//Inicializar variables
	bool conexion;
	sf::Packet pck;
	PlayerInfo playerInfo;
	std::vector<sf::TcpSocket*> vSockets;

	//Inicializar socket,listener y abrir puerto
	sf::TcpSocket sock;
	sf::TcpListener listener;
	sf::TcpSocket::Status status = listener.listen(50002);

	//Checkear status de la conexion
		//Conexion falla
	if (status == sf::TcpSocket::Status::Error)
	{
		conexion = false;
		return 0;
	}
		//Conexion exitosa
	else if (status == sf::TcpSocket::Status::Done)
	{
		conexion = true;
		status = listener.accept(sock);
		std::cout << "conexion";

		if (status == sf::TcpSocket::Status::Done) 
		{
			//push al vector de sockets
			vSockets.push_back(&sock);
			
			//thread&detach
			std::thread t(&threadFunction, &sock);
			t.detach();

			conexion = true;
			std::cout << "conexion";
		}
		else return 0;			
	}

	if (conexion) 
	{
		while (true)
		{
			//DibujaSFML();
		}
	}
	
	return 0;
}