#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <thread>
#include <mutex>
#include <PlayerInfo.h>

#define MAX 100
#define SIZE_TABLERO 64
#define SIZE_FILA_TABLERO 8
#define LADO_CASILLA 64
#define RADIO_AVATAR 25.f
#define OFFSET_AVATAR 5

#define SIZE_TABLERO 64
#define LADO_CASILLA 64
#define RADIO_AVATAR 25.f
#define OFFSET_AVATAR 5



sf::Vector2f BoardToWindows(sf::Vector2f _position)
{
	return sf::Vector2f(_position.x*LADO_CASILLA + OFFSET_AVATAR, _position.y*LADO_CASILLA + OFFSET_AVATAR);
}

void DibujaSFML()
{
	sf::Vector2f casillaOrigen, casillaDestino;
	bool casillaMarcada = false;

	sf::RenderWindow window(sf::VideoMode(512, 512), "Ejemplo tablero");
	while (window.isOpen())
	{
		sf::Event event;

		//Este primer WHILE es para controlar los eventos del mouse
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			default:
				break;

			}
		}

		window.clear();

		//A partir de aquÃ­ es para pintar por pantalla
		//Este FOR es para el tablero
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				sf::RectangleShape rectBlanco(sf::Vector2f(LADO_CASILLA, LADO_CASILLA));
				rectBlanco.setFillColor(sf::Color::White);
				if (i % 2 == 0)
				{
					//Empieza por el blanco
					if (j % 2 == 0)
					{
						rectBlanco.setPosition(sf::Vector2f(i*LADO_CASILLA, j*LADO_CASILLA));
						window.draw(rectBlanco);
					}
				}
				else
				{
					//Empieza por el negro
					if (j % 2 == 1)
					{
						rectBlanco.setPosition(sf::Vector2f(i*LADO_CASILLA, j*LADO_CASILLA));
						window.draw(rectBlanco);
					}
				}
			}
		}

		//Para pintar el un circulito
		sf::CircleShape shape(RADIO_AVATAR);
		shape.setFillColor(sf::Color::Blue);
		sf::Vector2f posicion_bolita(4.f, 7.f);
		posicion_bolita = BoardToWindows(posicion_bolita);
		shape.setPosition(posicion_bolita);
		window.draw(shape);
		window.display();
	}

}

std::mutex mtx;

void threadFunction(sf::TcpSocket* sock, std::vector<std::string> *v)
	{
		while (true)
		{
			sf::Packet pcket;
			sock->receive(pcket);
			sf::String str;
			pcket >> str;
			mtx.lock();
			v->push_back(str);
			mtx.unlock();
		}

	}

	int main()
	{
		
		sf::TcpSocket sock;
		sf::TcpSocket::Status status = sock.connect("192.168.1.150", 50002, sf::seconds(15.f)); //puerto 
		sf::Packet pck;
		PlayerInfo playerInfo;

		if (status == sf::TcpSocket::Status::Error)
		{
			return 0;
		}
		else if (status == sf::TcpSocket::Status::Done)
		{
			pck << "test";
			status = sock.send(pck);
			std::cout << "conexion";
		}

		std::vector<std::string> aMensajes;
		sf::Vector2i screenDimensions(800, 600);

		sf::RenderWindow window;
		window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

		sf::Font font;
		if (!font.loadFromFile("courbd.ttf"))
		{
			std::cout << "Can't load the font file" << std::endl;
		}

		
		//*********THREAD*********//
		std::thread t(&threadFunction, &sock, &aMensajes);
		//************************//

		
			/*while (window.isOpen())
			{
				sf::Event evento;
				while (window.pollEvent(evento))
				{
					switch (evento.type)
					{
					case sf::Event::Closed:
						window.close();
						break;
					case sf::Event::KeyPressed:
						if (evento.key.code == sf::Keyboard::Escape)
							window.close();
						else if (evento.key.code == sf::Keyboard::Return)
						{
							pck << mensaje;
							sock.send(pck);
							mtx.lock();
							aMensajes.push_back(mensaje);
							mtx.unlock();


							if (aMensajes.size() > 25)
							{
								aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
							}
							mensaje = ">";
						}
						break;
					case sf::Event::TextEntered:
						if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
							mensaje += (char)evento.text.unicode;
						else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
							mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
						break;
					}
				}
				window.draw(separator);
				for (size_t i = 0; i < aMensajes.size(); i++)
				{
					std::string chatting = aMensajes[i];
					chattingText.setPosition(sf::Vector2f(0, 20 * i));
					chattingText.setString(chatting);
					window.draw(chattingText);
				}
				std::string mensaje_ = mensaje + "_";
				text.setString(mensaje_);
				window.draw(text);


				window.display();
				window.clear();
			}*/
		}
		
		


	